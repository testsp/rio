var express = require('express');
var router = express.Router();

//*****************************************************************************************//
//		Bibliotecas
//*****************************************************************************************//

//---------------------------------------//
//		Externas
//---------------------------------------//

// gerador de UUID
var uuid = require('shortid');


//*****************************************************************************************//
//		Serviços Nuvem
//*****************************************************************************************//

//---------------------------------------//
//		Log Management
//---------------------------------------//

// Logentries
var LogentriesClient = require('logentries-client');

// Credenciais Log Back_end
var log = new LogentriesClient({
    token: 'xxx', 
	timestamp : true,
	withStack : true
});

// Dirigimos Error de Logentries 
log.on('error',function(err){ 
	console.log('LE ERROR', err);
});


//*****************************************************************************************//
//		DB-Link
//*****************************************************************************************//

//------------------------------------------------//
//		MongoDB
//------------------------------------------------//

// Bring Mongoose into the project
var mongoose = require( 'mongoose' );

// Credenciais de (BD) en MongoLab - (Test)
var dbURI = 'xxx';	

// Create the database connection
mongoose.connect(dbURI);

//-----------------------//
//	  Events Mongoose
//-----------------------//

// Conexion MongoDB
mongoose.connection.on('connected', function () {
	
	// Guardamos evento em Log
	log.log('info', {source : 'BD', 
					 BD : 'MongoDB' ,
					 funcion : 'mongoose.connection.on' , 
					 evento : 'Mongoose connected to ' + dbURI});	
});

// Error em Conexion MongoDB
mongoose.connection.on('error',function (err) {	
	
	// Guardamos Error em Log
	log.log('err', {source : 'BD', 
					BD : 'MongoDB' ,
					funcion : 'mongoose.connection.on' , 
					evento : 'Mongoose connection error: ' , 
					error : err});	
		
	// Re-Conexion MongoDB
	mongoose.connection.on('connected', function () {
		
		// Guardamos evento no Log
		log.log('info', {source : 'BD', 
						 BD : 'MongoDB' ,
						 funcion : 'Re-Conexion MongoDB' , 
						 evento : 'Mongoose connected to ' + dbURI});	
	});
					
});


//------------------------------------------//
//			Define - User schema 
//------------------------------------------//


// Esquema Usuario
var userSchema = new mongoose.Schema({
    codigo  :   Number,
	nome	:	String,
	email	:  	String,
	cpf		:	String,
	saldo	:	String,
	endereco: {
		tipo:			String,
		logradouro:		String,
		numero:			String,
		complemento:	String,
		bairro:	String,
		cidade:	String,
		Estado:	String,
		Pais:	String
	},
	telefone: {
		pais:	String,
		area:	String,
		numero:	String
	},
	thumb	:	String,
	obs	:	String
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'  }});


// Esquema Usuario
var infoSchema = new mongoose.Schema({
    user_id  :   String,
	saldo	:	Number,
	total	:  	Number,
	utilizados		:	Number,
	expirados	:	Number,
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'  }});



// Esquema Usuario
var lojaSchema = new mongoose.Schema({
	loja : String,
	numero: String,
	data: Date,
	pagamento:String,
	valor : Number
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'  }});


//------------------------------------------//
//			Build the Car model 
//------------------------------------------//

var user_info = mongoose.model( 'user_info', userSchema );

var tinfo = mongoose.model( 'info', infoSchema );

var loja = mongoose.model( 'loja', lojaSchema );




//*****************************************************************************************//
//		Fin execução APP
//*****************************************************************************************//

//------------------------------------------------//
//	  Eventos NodeJS
//------------------------------------------------//

// Fim de execução con [CTRL+C]
process.on('SIGINT', function() { 
	
	// Guardamos Fim de App en Log
	log.log('notice', {source : 'NodeJS', 
					  funcion : 'SIGINT' , 
					  evento : 'app termination'});	
	
	console.log('app termination');
	
	// fechamos Cliente Mongoose
	mongoose.connection.close();
	
	// Delay de 2 Seg.
	setTimeout( function() { 
		
		// fechamos conexion Logentries
		log.end();
		
		// Code Exit (0) - Ok
		process.exit(0); 
		
	} , 2000);

});

// Fim de execução por Error Sem manejador
process.on('uncaughtException', function (err) {
	
	// Guardamos Error em Log
	log.log('err', {source : 'NodeJS', 
					funcion : 'uncaughtException' , 
					evento : 'Errores Sin manejador' , 
					error : err});	
	
	console.log('app termination uncaughtException');
	
	// fechamos Cliente Mongoose
	mongoose.connection.close();
	
	// Delay de 2 Seg.
	setTimeout( function() { 
	
		// fechamos conexion Logentries
		log.end();
		
		// Code Exit (1) - (Uncaught Fatal Exception)
		process.exit(1); 
		
	} , 2000);
  
});



//*****************************************************************************************//
//		Inicio de APP
//*****************************************************************************************//

console.log('app start');

// Log Inicio App
log.log('info', { source : 'App - pru', 
				  evento : 'app start'});	


//*****************************************************************************************//
//		Logica
//*****************************************************************************************//

//--------------------------------------------------------------//
//		funções 
//--------------------------------------------------------------//

router.allUser = function(req, res){	// getAllVeiculos

	
	// pesquisamos Users
	user_info.find( {} 
	,function(err,Users) {
		
		if (!err) {	// Sim Não error (BD) - MongoDB
		   
			if (Users.length == 0) {	// Sem Users obtidos
				
				// Resposta ao Front
				res.status(200).send({ error: 'msg_code_17', Users: [] });
							
			}else{	// Com Users obtidos
				// Resposta ao sFront
				res.status(200).send({ message: 'msg_code_16' , Users: Users });
			
			}
		
		}else{	// Com error (BD) - MongoDB
		
			// Guardamos Error em Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'busqItems' , 
							evento 	: 'pesquisamos Users' , 
							error 	: err});	
			
			// Respuesta a Front
			res.status(400).send({ error: 'msg_code_17'});
			
		}
		
	});

};

router.idUser = function(req, res){	// Buscamos Users pelo ID

	// Buscamos Users pelo ID
	tinfo.find( {"user_id" : req.params.id}
	,function(err,item) {
		
		if (!err) {	// Sem error (BD) - MongoDB
		
			if (item.length == 0) {	// Item Não obtido
				
				// Respuesta al Front
				res.status(200).send({ error: 'msg_code_17', info: [] });
							
			}else{	// Users obtido
				
				// Respuesta ao Front
				res.status(200).send({ message: 'msg_code_16' , info: item[0] });
			
			}
		
		}else{	// Com error (BD) - MongoDB
		
			// Guardamos Error no Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'busqItems' , 
							evento 	: 'Buscamos Users pelo ID' , 
							error 	: err});	
			
			// Respuesta a Front
			res.status(400).send({ error: 'msg_code_17'});
			
		}
		
	});
	
};

router.addNotas = function(req, res){	// Adiccionar Users

	// data User
 	var notas	= req.body.notas;		// pegamos [nome]
	
	// Os dados do formulário são validados
	if (   null == notas 		|| notas.length < 1 
		) {
		
		res.status(400).send({ error : 'Error Datos del Formulario' });
		return;
	}
	


	notas.forEach(function(element) {
    	// Criamos o User em MongoDB
		loja.create({
			loja : element.loja,
			numero: element.numero,
			data: element.data,
			pagamento:element.pagamento,
			valor : element.valor
		},function(err,User) {
			
			if (!err) {	// Sem error (BD) - MongoDB
			
				if (User) {	// Se User Creado
					
					// Respuesta al Front
					//res.status(200).send({ message: 'msg_code_2' });
				}
				
			}else{	// Com error (BD) - MongoDB
			
				// Guardamos Error no Log
				log.log('err', {source 	: 'DB', 
								BD 		: 'MongoDB' , 
								funcion : 'addItem' , 
								evento 	: 'Adiccionar User' , 
								error 	: err});	
				
				// Respuesta ao Front
				//res.status(400).send({ error: 'Error'});
				
			}
		});
    	
	});
	
	res.status(200).send({ message: 'msg_code_2' });
	
};

router.getNotas = function(req, res){	// getAllVeiculos

	
	// pesquisamos Users
	loja.find( {} 
	,function(err,lojas) {
		
		if (!err) {	// Sim Não error (BD) - MongoDB
		   
			if (lojas.length == 0) {	// Sem lojas obtidos
				
				// Resposta ao Front
				res.status(200).send({ error: 'msg_code_17', lojas: [] });
							
			}else{	// Com lojas obtidos
				// Resposta ao sFront
				res.status(200).send({ message: 'msg_code_16' , lojas: lojas });
			
			}
		
		}else{	// Com error (BD) - MongoDB
		
			// Guardamos Error em Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'busqItems' , 
							evento 	: 'pesquisamos Users' , 
							error 	: err});	
			
			// Respuesta a Front
			res.status(400).send({ error: 'msg_code_17'});
			
		}
		
	});

};
	
//----------------------//
//----------------------//

module.exports = router;