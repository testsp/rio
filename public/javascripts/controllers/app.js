var module = ons.bootstrap('my-app', ['onsen',
                                      'ngStorage'
                                      ]); 

 
//-----------------------------------------------------------------------------------------//
//    	Directivas
//-----------------------------------------------------------------------------------------//

// lodash support
module.constant('_', window._);

 
 
//-----------------------------------------------------------------------------------------//
//    	controller
//-----------------------------------------------------------------------------------------//

module.controller('AppController', function($scope, 
                                            $timeout,
                                            $localStorage,
                                            $http,
                                            $filter,
                                            _
                                            ) {

// lodash support
$scope._ = _;


	
	$scope.changePage = function(page) {
  	
  		document.getElementById('mySplitter').content.load(page)
        .then(function() {
          document.getElementById('mySplitter').left.close();
        });
	}
	
	
	if(!$scope.entry){
		$scope.entry = 1;
		
		$timeout(function() {
	        $scope.$apply(function(){ 
        		console.log('tabs')
	        	$(document).ready(function(){
					$('ul.tabs').tabs();
				});
        	});
    	},10000); 
	}
   
  
  
//*****************************************************************************************************************//
//    	Propriedades
//*****************************************************************************************************************//
    
//---------------------------------//
//    	ng-Storage
//---------------------------------//
    
$scope.$storage = $localStorage.$default({
	
	items : [],
	notas : [],
	info : {
		saldo: '',
        total: '',
    	utilizados: '',
    	expirados: ''
	},
	index : 0,
    // Dados do novo usuario para cadastro 
	Item: {
			loja : '',
			numero: '',
			data: '',
			pagamento:'Selecionar',
			valor : ''
	}, 
	lojas : []
    
});

// page flag que indica em que tab esta
// search flag que indica si se esta buscando um usuario
$scope.option = {page : 2, search :0};

$scope.$storage.info =  {
		saldo: '',
        total: '',
    	utilizados: '',
    	expirados: ''
		};
		
		
$scope.$storage.Item = {
			loja : '',
			numero: '',
			data: '',
			pagamento:'Selecionar',
			valor : ''
	};

$scope.$storage.notas = [];
 
 

 


 
//------------------------------------//
//			Variables Msj
//------------------------------------// 

$scope.aviso = [];
     
//------------------------------------//
//        Metodos dialog, alerts
//------------------------------------//

    // alert geral para notificar que uma operacão teve erro
    $scope.alert = function(msj) {
		ons.notification.alert({ 'title':msj.titulo , 'message': msj.mensaje });
    }
    
    // função que mostra o dialog
    $scope.dialogLoadShow = function(dialog,mensaje){
        // aloca o mensagem a mostrar no alert
        $timeout(function() {
            $scope.$apply(function(){ 
			
                     $scope.$storage.mensaje = mensaje;
                     
                     // mostra o dialog
                     document.getElementById(dialog).show();
            });
        },0);                 
        
    };
    
	// função que oculta o dialog
    $scope.dialogLoadHide = function(dialog){
		// muestra el dialog
        $timeout(function() {
            $scope.$apply(function(){ 
		        document.getElementById(dialog).hide();
            });
        },0); 
    };

//------------------------------------//
//        Metodos de validacão do Ano
//------------------------------------//
	
	$scope.validateForm = function(){
		
		
		
		var flag = false;	
		
		if (angular.isDefined($scope.$storage.Item) &&
			angular.isDefined($scope.$storage.Item.data) &&
			angular.isDefined($scope.$storage.Item.pagamento) &&
			angular.isDefined($scope.$storage.Item.numero) &&
			angular.isDefined($scope.$storage.Item.loja) &&
			angular.isDefined($scope.$storage.Item.valor) ){
		
			if($scope.$storage.Item.pagamento == "Selecionar"){
				
				flag = true
			} 
		 
		 
			
				// entre 1900 e año atual	
			else if($scope.$storage.Item.data.getFullYear() < 1900 || $scope.$storage.Item.data.getFullYear() > (new Date()).getUTCFullYear()) {
				
		    	flag = true;
            
			}
			
			else if($scope.$storage.Item.valor < 0){
				flag = true;
			}
			
			else if($scope.$storage.Item.numero < 0){
				flag = true;
			}
			
		}
				
		else{ // Não Error
			 
		     flag = true;
		}
		
		
				
		return flag
		
	}
	
	
//------------------------------------//
//			GetAll User
//------------------------------------// 

	$scope.getTel = function(){
		return '+ ' + $scope.$storage.items[$scope.$storage.index].telefone.pais + ' ' + $scope.$storage.items[$scope.$storage.index].telefone.area + ' ' + $scope.$storage.items[$scope.$storage.index].telefone.numero;
	}
	
	
	$scope.getEndereco = function(){
		return $scope.$storage.items[$scope.$storage.index].endereco.logradouro + ', ' + $scope.$storage.items[$scope.$storage.index].endereco.numero +  ' ' + $scope.$storage.items[$scope.$storage.index].endereco.cidade + ', ' + $scope.$storage.items[$scope.$storage.index].endereco.cidade +  ', ' + $scope.$storage.items[$scope.$storage.index].endereco.Estado;
	}


	$scope.getLojas = function(){
 
		$scope.option.page = 4;
		
		// Mostramos Icono de Procesamento
	    $scope.dialogLoadShow('dialog-1',':)');
		
		$http.get("/lojas")
		.then( function (data) {	// Sem Error desde o Servidor
			
		    // Escondemos Icono de Procesamento
			$scope.dialogLoadHide('dialog-1');
			
			// armazena os dados
			$scope.$storage.lojas = data.data.lojas;
			
		
		},
		function (err) {	// Com Error desde o Servidor  
			
			// fecha Icono de Procesamento
			$scope.dialogLoadHide('dialog-1');
			
			// Carregamos mensagem para o alert
			$scope.aviso = {};
			$scope.aviso.titulo = 'Error';
			$scope.aviso.mensaje = 'Nao se pode obter os dados';
			
			// Mostra alert
			$scope.alert($scope.aviso);
		    
		});
		
	}
 
	// função que obtem tudos os user
	$scope.getAllUser = function(){
		
		// Mostramos Icono de Procesamento
	    $scope.dialogLoadShow('dialog-1',':)');
		
		$http.get("/cliente")
		.then( function (data) {	// Sem Error desde o Servidor
			
		    // Escondemos Icono de Procesamento
			$scope.dialogLoadHide('dialog-1');
			
			// armazena os dados
			$scope.$storage.items = data.data.Users;
			
		
		},
		function (err) {	// Com Error desde o Servidor  
			
			// fecha Icono de Procesamento
			$scope.dialogLoadHide('dialog-1');
			
			// Carregamos mensagem para o alert
			$scope.aviso = {};
			$scope.aviso.titulo = 'Error';
			$scope.aviso.mensaje = 'Nao se pode obter os dados';
			
			// Mostra alert
			$scope.alert($scope.aviso);
		    
		});
			
	}
	
	// get all user
	$scope.getAllUser();
	
//------------------------------------//
//        Metodos ADD, DELETE, APDATE, GET
//------------------------------------//

	$scope.addNota = function(){
		//insere o novo item
	 
		$scope.$storage.notas.push($scope.$storage.Item);
		
		$scope.$storage.Item = {
			loja : '',
			numero: '',
			data: '',
			pagamento:'Selecionar',
			valor : 0
		};
		
		
	}
	
	$scope.resetTab = function(tab){
		$timeout(function() {
	        $scope.$apply(function(){ 
		         $(document).ready(function(){
					UIkit.tab($('[uk-tab]')).show(tab);
				});
	        });
	    },0); 
	}
	
	
	$scope.getInfo = function(id,index){ 
		
		
		$scope.option.search = 1;
		$scope.option.page = 1;
		
		
		// carrega tab 0
		$scope.resetTab(0);
		
		
		// limpa as notas
		$scope.$storage.notas = [];
	   
		
		// index do item
		$scope.$storage.index = index;
		
		// imagem do perfil
		$scope.thumb = $scope.$storage.items[index].thumb
		
		
		// Mostramos Icono de Procesamento
	    $scope.dialogLoadShow('dialog-1',':)');
	    
	    var requestUrl = '/cliente/' + id ;
		
		$http.get(requestUrl)
		.then( function (data) {	// Sem Error desde o Servidor
			
		    // Escondemos Icono de Procesamento
			$scope.dialogLoadHide('dialog-1');

			
			// armazena os dados
			$scope.$storage.info = data.data.info;
			
		
		},
		function (err) {	// Com Error desde o Servidor  
			
			// fecha Icono de Procesamento
			$scope.dialogLoadHide('dialog-1');
			
			// Carregamos mensagem para o alert
			$scope.aviso = {};
			$scope.aviso.titulo = 'Error';
			$scope.aviso.mensaje = 'Nao se pode obter os dados';
			
			// Mostra alert
			$scope.alert($scope.aviso);
		    
		});
	}

 
	$scope.clearData = function(){
		$scope.option.search = 0
		$scope.option.page = 2;
		
		// carrega tab 1
		$scope.resetTab(1);
		
		$scope.$storage.info =  {
		saldo: '',
        total: '',
    	utilizados: '',
    	expirados: ''
		};
		
	}
	
	$scope.deleteNota = function(index){
		// Eliminamos nota
		$scope.$storage.notas.splice(index, 1);
	}
	
	
	$scope.postInfo = function(){
		// Mostramos Icono de Procesamento
		$scope.dialogLoadShow('dialog-1',':)');

		$http.post("/addnotas", {notas : $scope.$storage.notas})
		.then( function (data) {	// Sem Error desde o Servidor
			
            // fechamos Icono de Procesamento
			$scope.dialogLoadHide('dialog-1');
			
			// limpa as notas
			$scope.$storage.notas = [];
		 
        
		},
	    function (err) {	// Com Error desde o Servidor
			
			// fechamos Icono de Procesamento
			$scope.dialogLoadHide('dialog-1');
			
			// Carregamos mensagem para o alert
			$scope.aviso = {};
			$scope.aviso.titulo = 'Error';
			$scope.aviso.mensaje = 'Error tentando Inserir';
			
			// Mostramos o alert
			$scope.alert($scope.aviso);
            
		});
	}
		
})
